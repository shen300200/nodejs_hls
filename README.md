# nodejs_hls

#### 介绍
监控rtsp流视频在web页面播放

#### 软件架构
用 nodejs 做后台，控制 ffmpeg.exe 拉流和推流。
前端页面使用 vediojs 插件播放视频。


#### 安装教程

1.  下载代码后解压 ffmpeg.zip 到当前目录下(ffmpeg.exe与ffmpeg.zip同目录，解压后可删除ffmpeg.zip)。
2.  下载安装nodejs，官网 [http://nodejs.cn/download/](http://nodejs.cn/download/)。
3.  在当前目录下打开终端，执行 `npm install` 命令安装依赖。
4.  然后执行 `node index.js` 开启本地服务(或者双击start.bat)。出现如下信息则说明开启成功，关闭该窗口则会停止服务。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/110635_c8484dad_1805492.jpeg "1.jpg")

#### 使用说明

1.  开启本地服务后在浏览器输入地址 http://localhost:2000/ 可打开控制页面如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/110652_e59de9c7_1805492.jpeg "2.jpg")
2.  在该页面添加视频监控配置信息，然后点击“启动设置”加载配置信息。
3.  启动设置成功后，可在下面输入转换后的视频地址测试播放。配置中的key值对应视频的名称。
